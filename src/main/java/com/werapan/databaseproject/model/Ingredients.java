/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class Ingredients {
    private int IngId;
    private String IngName ;
    private float IngPrice;
    private String IngType;
    private int IngQoh;
    private int IngMin;

    public Ingredients(int IngId, String IngName, float IngPrice, String IngType, int IngQoh, int IngMin) {
        this.IngId = IngId;
        this.IngName = IngName;
        this.IngPrice = IngPrice;
        this.IngType = IngType;
        this.IngQoh = IngQoh;
        this.IngMin = IngMin;
    }
    
    public Ingredients(String IngName, float IngPrice, String IngType, int IngQoh, int IngMin) {
        this.IngId = -1;
        this.IngName = IngName;
        this.IngPrice = IngPrice;
        this.IngType = IngType;
        this.IngQoh = IngQoh;
        this.IngMin = IngMin;
    }
    
    public Ingredients() {
        this.IngId = -1;
        this.IngName = "";
        this.IngPrice = 0;
        this.IngType = "";
        this.IngQoh = 0;
        this.IngMin = 0;
    }

    public int getIngId() {
        return IngId;
    }

    public void setIngId(int IngId) {
        this.IngId = IngId;
    }

    public String getIngName() {
        return IngName;
    }

    public void setIngName(String IngName) {
        this.IngName = IngName;
    }

    public float getIngPrice() {
        return IngPrice;
    }

    public void setIngPrice(float IngPrice) {
        this.IngPrice = IngPrice;
    }

    public String getIngType() {
        return IngType;
    }

    public void setIngType(String IngType) {
        this.IngType = IngType;
    }

    public int getIngQoh() {
        return IngQoh;
    }

    public void setIngQoh(int IngQoh) {
        this.IngQoh = IngQoh;
    }

    public int getIngMin() {
        return IngMin;
    }

    public void setIngMin(int IngMin) {
        this.IngMin = IngMin;
    }

    @Override
    public String toString() {
        return "Ingredients{" + "IngId=" + IngId + ", IngName=" + IngName + ", IngPrice=" + IngPrice + ", IngType=" + IngType + ", IngQoh=" + IngQoh + ", IngMin=" + IngMin + '}';
    }
    
     public static Ingredients fromRS(ResultSet rs) {
        Ingredients ingredients = new Ingredients();
        try {
            ingredients.setIngId(rs.getInt("ingredients_id"));
            ingredients.setIngName(rs.getString("ingredients_name"));
            ingredients.setIngPrice(rs.getFloat("ingredients_price"));
            ingredients.setIngType(rs.getString("ingredients_type"));
            ingredients.setIngQoh(rs.getInt("ingredients_qoh"));
            ingredients.setIngMin(rs.getInt("ingredients_min"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Ingredients.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ingredients;
    }
    

}
