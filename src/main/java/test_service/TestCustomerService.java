/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author Asus
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        Customer updatecus = cs.getByTel("1234567890");
        updatecus.setTel("0100000000");
        cs.update(updatecus);
        for (Customer customer : cs.getCustomers() ){
            System.out.println(customer);
        }
         System.out.println("");
    }
} 
